"use strict";
class InMemoryDataService {
    createDb() {
        let contatos = [
            { id: 1, nome: "fulano", email: "fulano@email.com", telefone: "8888-9999" },
            { id: 2, nome: "madruga", email: "madruga@email.com", telefone: "5555-9999" },
            { id: 3, nome: "chaves", email: "chaves@email.com", telefone: "4444-9999" },
            { id: 4, nome: "kiko", email: "kiko@email.com", telefone: "3333-9999" },
            { id: 5, nome: "chiquinha", email: "chiquinha@email.com", telefone: "2222-9999" }
        ];
        return { contatos: contatos };
    }
}
exports.InMemoryDataService = InMemoryDataService;
//# sourceMappingURL=in-memory-data.service.js.map